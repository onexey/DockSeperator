//
//  main.m
//  DockSeperator
//
//  Created by Mesut Soylu on 17/05/16.
//  Copyright © 2016 Mesut Soylu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
